# Tutoriels LP CISIIE

## module html avancé

Construire un tutoriel basé sur une démo sur un des thèmes listés ci-dessous.
Chaque tutoriel correspond à un des répertoires du projet.
Le tutoriel en lui-même, ou un lien, doit être décrit dans le fichier README.md du répertoire, en utilisant la syntaxe markdown.


## thèmes prévus : 
1. css transition : Jordan Becker (2), Jacques Cornat (3), Julien Michel (4)
2. css colors : Timothée Olivar (5), Jules Mougin (6), François Lacercat (7)
3. filter effects : Guillaume Migeon (8), Nicolas Obara (1), Christophe Sauder (2)
4. multiple column layout : Guillaume Degesne (3), Grégory Baton (5), Lucas Gehin (6)
5. blending mode : Théo Gerriet (7), Alexis Magron (8), Brian Nallamoutou (1)
6. css animation : Loïc L'Etang (2), Hector Trujillo Ruiz (3), Kamel Remaki (4)
7. css transform : Antoibe Dautreville (5), Florian Fabing (6), Jordan Jung (8)
8. @font-face & webfonts : Thomas Frantz (1), David Jacquot (2)

(entre parenthèses, pour chaque étudiant, les n° des tutos qu'il doit évaluer).

## Utilisation du dépôt

**Ne travaillez pas directement sur ce dépôt**

Faites un fork pour créer un dépôt dont vous êtes propriétaire.

Utilisez ce nouveau dépôt : clonez-le sur votre machine. 

**Créez une branche portant le nom de votre tuto !**

Faites vos commits/push sur votre dépôt. Quand votre tutoriel est prêt, faites un pull request pour l'intégrer dans ce dépôt.


