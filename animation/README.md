CSS-animation

Membres : Loïc L'Etang, Hector Trujillo Ruiz, Kamel Remaki

Pré-requis :

	Afin d'utiliser la propriété CSS3 « CSS-animation » , il faut que votre browser internet soit supérieur au version dans le tableau ci-dessous : 

Property
ie
chrome
firefox
safari
opera
@keyframes
10.0
4.0 -webkit-
16.0
5.0 -moz-
4.0 -webkit-
15.0 -webkit-
12.1
12.0 -o-
animation
10.0
4.0 -webkit-
16.0
5.0 -moz-
4.0 -webkit-
15.0 -webkit-
12.1
12.0 -o-


Conception : 

1) @Keyframes
	Dans un premier temps, vous allez devoir créer une keyFrames qui va définir votre animation. Pour cela voici la sémantique de cette règle CSS :


@keyframes nomDeVotreAnimation{
	0%{
		reglecss:attribut ;
	}
	x%{
		reglecss:attribut ;
	}  
	ou
	from{
		reglecss:attribut ;
	}
	to{
		reglecss:attribut ;
	}

Les variables de pourcentages varie entre 0 % et 100 %, vous pouvez mettre autant de règle avec des pourcentages que vous voulez ( 0 %-10 %-100 % / 0 %-25 %-50 %-100 % … ).
Voici un exemple avec les deux sémantiques : 

1)
@keyframes resize {
  0% {
    padding: 0;
  }
  50% {
    padding: 0 20px;
    background-color:rgba(255,0,0,0.2);
  }
  100% {
    padding: 0 100px;
    background-color:rgba(255,0,0,0.9);
  }
}
2)
@keyframes resize {
  from{
    padding: 0;
  }
  to {
      padding: 0 100px;
    	background-color:rgba(255,0,0,0.9);
 }
}


Le premier exemple a beaucoup plus de transition que le deuxième, à vous de choisir selon l'utilisation de l'animation la première ou la deuxième sémantique.

2) implementation de css-animation 



Voici un exemple d'utilisation de la propriété css animation : 

#selecteur{
  animation-name: nomDeVotreAnimation;
  animation-duration: durée de votre animation (en secondes,minutes,heures);
  animation-iteration-count: nombreDiterationDeVotreAnimation;
  animation-direction: directionDeVotreAnimation;
  animation-timing-function: Rapidité de votre animation;
}
Vous avez en annexe le descriptif de toutes les propriétés de css-animation (obligatoire ou non ) et leurs valeurs :
























ANNEXE
animation-delay
Temps entre chaque transition  en secondes ou minutes ou heures 
Exemples : 1s | 2m | 3h

Syntax
Formal syntax: <time>#
animation-delay: 3s
animation-delay: 2s, 4ms

animation-direction

Value
Description

normal
Valeur par defaut . L'animation se jouera comme à la normal

reverse
L'animation se jouera dans la direction inverse

alternate
L'animation va être jouée à la normal pendant un temps (1,3,5,etc..) et va être jouée ensuite dans la direction inverse pendant un autre temps  (2,4,6,etc...)

alternate-reverse
L'animation va être jouée dans la direction inverse de la direction normal pendant un temps (1,3,5,etc..) et va être jouée ensuite dans la direction normal pendant un autre temps  (2,4,6,etc...)

initial
Valeur par default

inherit
Herite de l'élement parents


animation-duration
Durée de l'animation en secondes ou minutes ou heures .

Syntax
animation-duration: 120ms
animation-duration: 1s, 15s
animation-duration: 10s, 30s, 230ms


animation-fill-mode

Value
Description
none
Valeur par default. Aucun style appliqué
forwards
Après la fin de l'animation (déterminé par animation-iteration-count), l'animation va appliqué la valeur de la propriété pour la fin du temps de l'animation.
backwards
The animation will apply the property values defined in the keyframe that will start the first iteration of the animation, during the period defined by animation-delay. These are either the values of the from keyframe (when animation-direction is "normal" or "alternate") or those of the to keyframe (when animation-direction is "reverse" or "alternate-reverse")
both
The animation will follow the rules for both forwards and backwards. That is, it will extend the animation properties in both directions
initial
Valeur par default
inherit
Herite de l'élement parents


animation-iteration-count
Nombre d'iteration

Valeur initiale 1 
S'applique à tous les éléments, ainsi que les pseudo-elements::before et ::after 
Héritée non 
Pourcentages — 
Média visual 
Valeur :Valeur initiale 1 
S'applique à tous les éléments, ainsi que les pseudo-elements::before et ::after 
Héritée non 
Pourcentages — 
Média visual 
Valeur calculée comme spécifié 
Animable non 
Ordre canonique l'ordre unique et non-ambigu défini par la grammaire formelle


Syntaxe
Syntaxe formelle : <single-animation-iteration-count>#
animation-iteration-count: infinite
animation-iteration-count: 3
animation-iteration-count: 2.3

animation-iteration-count: 2, 0, infinite

animation-name
Votre keyFrames


animation-play-state

Value
Description

paused
L'animation est en pause

running
Valeur par default . Spécifie que l'animation est en cours

initial
Remet en valeur par default

inherit
Hérite de l’élément parent







animation-timing-function

Value
Description
Play it
linear
L'animation a la même vitesse du début à la fin

ease
Valeur par default . L'animation commence au ralenti, puis plus rapide et à la fin au ralenti

ease-in
L'animation est ralentie au début

ease-out
L'animation est ralentie à la fin

ease-in-out
L'animation est ralentie au début et à la fin



cubic-bezier(n,n,n,n)

Défini votre propre valeur pour la fonction  cubic-bezier 
Les valeurs possibles sont numériques de 0 à 1 

initial
Remet en valeur par default

inherit
Hérite de l’élément parent

